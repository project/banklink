<?php

/**
 * @file
 * Documentation for Banklink API.
 */

/**
 * Initiates the payment process.
 *
 * In order to start the payment process you need to invoke
 * banklink_start_transaction() with payment information passed in.
 *
 * This will redirect the user's browser to the payment gateway's webpage.
 *
 * @param array $payment
 *   An associative array of attributes describing the payment:
 *   - 'bank': Bank identifier (required).
 *      See banklink_get_defaults() for allowed identifiers.
 *   - 'amount': Required. Must me in format 12.34.
 *   - 'currency': Optional, defaults to EUR if not specified.
 *   - 'order_id': Optional, defaults to current timestamp if not specified.
 *   - 'lang': Optional, global default is used if not specified.
 *   - 'message': Optional, global default is used if not specified.
 *   - '...': You can add additional values to the $payment array
 *      which will persist throughout the payment process and will be available
 *      in the return data provided by banklink_get_response().
 */
function banklink_start_transaction(array $payment) {
  $_SESSION['banklink_payment'] = $payment;
  drupal_goto('banklink/payment');
}

/**
 * Invoked when user is redirected back from the payment gateway.
 *
 * When the user returns from a bank, the Banklink module
 * parses the return data and invokes hook_banklink_return()
 * with payment and response data passed in.
 *
 * In order to capture the resoponse and act on it, you must implement
 * hook_banklink_return() in your module.
 *
 * @param array $response
 *   An associative array of attributes describing the payment and it's status:
 *   - 'bank': Bank identifier.
 *   - 'status': Response status code.
 *   - 'verified': True if the response could be verified.
 *   - 'data': Raw response data received from the payment gateway.
 *   - 'success': True, if the payment was successful.
 *   - 'order_id': Order ID associated with the payment.
 *   - 'ref': Reference number associated with the payment.
 *   - 'amount': Amount paid.
 *   - 'payment': Payment data used to initiate the payment.
 */
function hook_banklink_return(array $response) {
  // Processing of the response.
}
