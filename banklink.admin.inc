<?php
/**
 * @file
 * Administration interface specific methods.
 */

/**
 * Defines a banklink settings form for administration interface.
 */
function banklink_settings_form($form_state) {
  $defaults = banklink_get_defaults();

  $form['banklink_lang'] = array(
    '#type' => 'select',
    '#title' => t('Preferred language'),
    '#options' => array(
      'EST' => t('Estonian'),
      'ENG' => t('English'),
      'RUS' => t('Russian')),
    '#default_value' => variable_get('banklink_lang', 'EST'),
  );
  $form['banklink_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment message'),
    '#default_value' => variable_get('banklink_message', t('Order @order_id')),
    '#description' => t('Use @order_id to represent actual order number.'),
  );

  $form['danskebank'] = array(
    '#type' => 'fieldset',
    '#title' => 'danskebank',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['danskebank']['banklink_danskebank_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_danskebank_active', 0),
  );
  $form['danskebank']['banklink_danskebank_plnet_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Route through pangalink.net'),
    '#default_value' => variable_get('banklink_danskebank_plnet_active', 0),
  );
  $form['danskebank']['banklink_danskebank_snd_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_danskebank_snd_id', ''),
  );
  $form['danskebank']['banklink_danskebank_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant private key'),
    '#default_value' => variable_get('banklink_danskebank_private_key', ''),
  );
  $form['danskebank']['banklink_danskebank_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Bank certificate'),
    '#default_value' => variable_get('banklink_danskebank_public_key', ''),
  );

  $form['krediidipank'] = array(
    '#type' => 'fieldset',
    '#title' => 'Krediidipank',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['krediidipank']['banklink_krediidipank_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_krediidipank_active', 0),
  );
  $form['krediidipank']['banklink_krediidipank_plnet_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Route through pangalink.net'),
    '#default_value' => variable_get('banklink_krediidipank_plnet_active', 0),
  );
  $form['krediidipank']['banklink_krediidipank_snd_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_krediidipank_snd_id', ''),
  );
  $form['krediidipank']['banklink_krediidipank_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant private key'),
    '#default_value' => variable_get('banklink_krediidipank_private_key', ''),
  );
  $form['krediidipank']['banklink_krediidipank_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Bank certificate'),
    '#default_value' => variable_get('banklink_krediidipank_public_key', ''),
  );

  $form['lhvpank'] = array(
    '#type' => 'fieldset',
    '#title' => 'LHV Pank',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['lhvpank']['banklink_lhvpank_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_lhvpank_active', 0),
  );
  $form['lhvpank']['banklink_lhvpank_plnet_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Route through pangalink.net'),
    '#default_value' => variable_get('banklink_lhvpank_plnet_active', 0),
  );
  $form['lhvpank']['banklink_lhvpank_snd_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_lhvpank_snd_id', ''),
  );
  $form['lhvpank']['banklink_lhvpank_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant private key'),
    '#default_value' => variable_get('banklink_lhvpank_private_key', ''),
  );
  $form['lhvpank']['banklink_lhvpank_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Bank certificate'),
    '#default_value' => variable_get('banklink_lhvpank_public_key', ''),
  );

  $form['maksekeskus'] = array(
    '#type' => 'fieldset',
    '#title' => 'Maksekeskus',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['maksekeskus']['banklink_maksekeskus_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_maksekeskus_active', 0),
  );
  $form['maksekeskus']['banklink_maksekeskus_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_maksekeskus_merchant_id', ''),
  );
  $form['maksekeskus']['banklink_maksekeskus_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret'),
    '#default_value' => variable_get('banklink_maksekeskus_api_secret', ''),
  );

  $form['maksekeskus_test'] = array(
    '#type' => 'fieldset',
    '#title' => 'Maksekeskus test',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['maksekeskus_test']['banklink_maksekeskus_test_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_maksekeskus_test_active', 0),
  );
  $form['maksekeskus_test']['banklink_maksekeskus_test_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_maksekeskus_test_merchant_id', ''),
  );
  $form['maksekeskus_test']['banklink_maksekeskus_test_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret'),
    '#default_value' => variable_get('banklink_maksekeskus_test_api_secret', ''),
  );

  $form['nordea'] = array(
    '#type' => 'fieldset',
    '#title' => 'Nordea',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['nordea']['banklink_nordea_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_nordea_active', 0),
  );
  $form['nordea']['banklink_nordea_plnet_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Route through pangalink.net'),
    '#default_value' => variable_get('banklink_nordea_plnet_active', 0),
  );
  $form['nordea']['banklink_nordea_rcv_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_nordea_rcv_id', ''),
  );
  $form['nordea']['banklink_nordea_merchant_mac_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant MAC key'),
    '#default_value' => variable_get('banklink_nordea_merchant_mac_key', ''),
  );
  $form['nordea']['banklink_nordea_hash_function'] = array(
    '#type' => 'textfield',
    '#title' => t('Hash function'),
    '#default_value' => variable_get('banklink_nordea_hash_function', ''),
  );

  $form['nordea_test'] = array(
    '#type' => 'fieldset',
    '#title' => 'Nordea Test',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['nordea_test']['banklink_nordea_test_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_nordea_test_active', 0),
  );
  $form['nordea_test']['banklink_nordea_test_rcv_id'] = array(
    '#type' => 'item',
    '#title' => t('Merchant ID'),
    '#markup' => '<pre>' . $defaults['nordea_test']['rcv_id'] . '</pre>',
  );
  $form['nordea_test']['banklink_nordea_test_merchant_mac_key'] = array(
    '#type' => 'item',
    '#title' => t('Merchant MAC key'),
    '#markup' => '<pre>' . $defaults['nordea_test']['merchant_mac_key'] . '</pre>',
  );
  $form['nordea_test']['banklink_nordea_test_hash_function'] = array(
    '#type' => 'item',
    '#title' => t('Hash function'),
    '#markup' => '<pre>' . $defaults['nordea_test']['hash_function'] . '</pre>',
  );

  $form['seb'] = array(
    '#type' => 'fieldset',
    '#title' => 'SEB',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['seb']['banklink_seb_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_seb_active', 0),
  );
  $form['seb']['banklink_seb_plnet_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Route through pangalink.net'),
    '#default_value' => variable_get('banklink_seb_plnet_active', 0),
  );
  $form['seb']['banklink_seb_snd_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_seb_snd_id', ''),
  );
  $form['seb']['banklink_seb_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant private key'),
    '#default_value' => variable_get('banklink_seb_private_key', ''),
  );
  $form['seb']['banklink_seb_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Bank certificate'),
    '#default_value' => variable_get('banklink_seb_public_key', ''),
  );

  $form['seb_test'] = array(
    '#type' => 'fieldset',
    '#title' => t('SEB Test'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['seb_test']['banklink_seb_test_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_seb_test_active', 0),
  );
  $form['seb_test']['banklink_seb_test_snd_id'] = array(
    '#type' => 'item',
    '#title' => t('Merchant ID'),
    '#markup' => '<pre>' . $defaults['seb_test']['snd_id'] . '</pre>',
  );
  $form['seb_test']['banklink_seb_test_private_key'] = array(
    '#type' => 'item',
    '#title' => t('Merchant private key'),
    '#markup' => '<pre>' . $defaults['seb_test']['private_key'] . '</pre>',
  );
  $form['seb_test']['banklink_seb_test_public_key'] = array(
    '#type' => 'item',
    '#title' => t('Bank certificate'),
    '#markup' => '<pre>' . $defaults['seb_test']['public_key'] . '</pre>',
  );

  $form['swedbank'] = array(
    '#type' => 'fieldset',
    '#title' => 'Swedbank',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['swedbank']['banklink_swedbank_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('banklink_swedbank_active', 0),
  );
  $form['swedbank']['banklink_swedbank_plnet_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Route through pangalink.net'),
    '#default_value' => variable_get('banklink_swedbank_plnet_active', 0),
  );
  $form['swedbank']['banklink_swedbank_snd_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('banklink_swedbank_snd_id', ''),
  );
  $form['swedbank']['banklink_swedbank_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Merchant private key'),
    '#default_value' => variable_get('banklink_swedbank_private_key', ''),
  );
  $form['swedbank']['banklink_swedbank_public_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Bank certificate'),
    '#default_value' => variable_get('banklink_swedbank_public_key', ''),
  );

  return system_settings_form($form);
}
