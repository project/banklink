<?php
/**
 * @file
 * Main module file containing hook definitions and business logic.
 */

/**
 * Implements hook_menu().
 */
function banklink_menu() {
  $items['admin/config/system/banklink'] = array(
    'title' => 'Banklink',
    'description' => 'Configure Banklink settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('banklink_settings_form'),
    'access arguments' => array('configure banklink'),
    'file' => 'banklink.admin.inc',
  );
  $items['banklink/payment'] = array(
    'title' => 'Starting payment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('banklink_payment_form'),
    'access callback' => TRUE,
  );
  $items['banklink/return/%'] = array(
    'page callback' => 'banklink_return',
    'page arguments' => array(1, 2),
    'access callback' => TRUE,
  );
  $items['banklink/cancel/%'] = array(
    'page callback' => 'banklink_return',
    'page arguments' => array(1, 2),
    'access callback' => TRUE,
  );
  $items['banklink/reject/%'] = array(
    'page callback' => 'banklink_return',
    'page arguments' => array(1, 2),
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function banklink_permission() {
  return array(
    'configure banklink' => array(
      'title' => t('Configure Banklink'),
      'description' => t('Allows an user to access Banklink configuration form.'),
    ),
  );
}

/**
 * Get a list of supported banks.
 */
function banklink_get_banks($active = TRUE) {
  $data = array();
  $defaults = banklink_get_defaults();

  foreach ($defaults as $bank => $default) {
    if (($active && variable_get("banklink_{$bank}_active")) || !$active) {
      $data[$bank] = $default['name'];
    }
  }

  return $data;
}

/**
 * Returns a standardized settings array for a given Bank.
 */
function banklink_get_settings($bank) {
  $defaults = banklink_get_defaults();

  $settings['bank'] = $bank;

  if (in_array($bank, array('maksekeskus', 'maksekeskus_test'))) {
    $settings['merchant_id'] = variable_get("banklink_{$bank}_merchant_id", '');
    $settings['api_secret'] = variable_get("banklink_{$bank}_api_secret", '');
  }
  else {
    $settings['snd_id'] = variable_get("banklink_{$bank}_snd_id", '');
    $settings['lang'] = variable_get("banklink_lang", 'EST');
    $settings['public_key'] = variable_get("banklink_{$bank}_public_key", '');
    $settings['private_key'] = variable_get("banklink_{$bank}_private_key", '');
  }

  $settings['active'] = variable_get("banklink_{$bank}_active", 0);
  $settings['name'] = $defaults[$bank]['name'];
  $settings['url'] = $defaults[$bank]['url'];
  $settings['return_url'] = url('banklink/return/' . $bank, array('absolute' => TRUE));
  $settings['reject_url'] = url('banklink/reject/' . $bank, array('absolute' => TRUE));

  if (!in_array($bank, array('danskebank', 'lhvpank', 'swedbank'))) {
    $settings['cancel_url'] = url('banklink/cancel/' . $bank, array('absolute' => TRUE));
  }

  switch ($bank) {
    case 'nordea':
      $settings['rcv_id'] = variable_get('banklink_nordea_rcv_id', '');
      $settings['hash_function'] = variable_get('banklink_nordea_hash_function', '');
      $settings['merchant_mac_key'] = variable_get('banklink_nordea_merchant_mac_key', '');
      break;

    case 'nordea_test':
      $settings['rcv_id'] = $defaults['nordea_test']['rcv_id'];
      $settings['hash_function'] = $defaults['nordea_test']['hash_function'];
      $settings['merchant_mac_key'] = $defaults['nordea_test']['merchant_mac_key'];
      break;

    case 'seb_test':
      $settings['snd_id'] = $defaults['seb_test']['snd_id'];
      $settings['public_key'] = $defaults['seb_test']['public_key'];
      $settings['private_key'] = $defaults['seb_test']['private_key'];
      break;

  }

  if (variable_get("banklink_{$bank}_plnet_active", 0)) {
    $settings['url'] = $defaults[$bank]['plnet_url'];
  }

  return $settings;
}

/**
 * Starts the transaction.
 *
 * Starts the payment process by storing the payment data in session
 * and redirectiong the user to the autosubmitting payment form.
 */
function banklink_start_transaction($payment) {
  $_SESSION['banklink_payment'] = $payment;
  drupal_goto('banklink/payment');
}

/**
 * Renders the autosubmittiong payment form.
 */
function banklink_payment_form($form) {
  $payment = $_SESSION['banklink_payment'];
  $settings = banklink_get_settings($payment['bank']);

  drupal_add_js('jQuery(document).ready(function () { jQuery(\'#banklink-payment-form\').submit(); });', array('type' => 'inline', 'scope' => 'footer'));

  $form['#action'] = $settings['url'];
  $form['message'] = array(
    '#markup' => t('You are being forwarded to the @name website. Please wait.', array('@name' => $settings['name'])),
  );

  if (in_array($payment['bank'], array('maksekeskus', 'maksekeskus_test'))) {
    banklink_maksekeskus_payment_form($form, $settings, $payment);
  }
  elseif (in_array($payment['bank'], array('nordea', 'nordea_test'))) {
    banklink_nordea_payment_form($form, $settings, $payment);
  }
  else {
    banklink_standard_payment_form($form, $settings, $payment);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array('style' => 'display: none'),
  );

  return $form;
}

/**
 * Prepares the form fields for the standard payment form.
 */
function banklink_standard_payment_form(&$form, $settings, $payment) {
  $order_id = $payment['order_id'] ?: REQUEST_TIME;

  $vk = array(
    'VK_SERVICE' => 1002,
    'VK_VERSION' => '008',
    'VK_SND_ID' => $settings['snd_id'],
    'VK_STAMP' => $order_id,
    'VK_AMOUNT' => number_format($payment['amount'], 2, '.', ''),
    'VK_CURR' => $payment['currency'] ?: 'EUR',
    'VK_REF' => banklink_get_reference_number($order_id),
    'VK_MSG' => $payment['message'] ?: strtr(variable_get('banklink_message', t('Order @order_id')), array('@order_id' => REQUEST_TIME)),
  );

  $form['VK_SERVICE'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_SERVICE'],
  );
  $form['VK_VERSION'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_VERSION'],
  );
  $form['VK_SND_ID'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_SND_ID'],
  );
  $form['VK_STAMP'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_STAMP'],
  );
  $form['VK_AMOUNT'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_AMOUNT'],
  );
  $form['VK_CURR'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_CURR'],
  );
  $form['VK_REF'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_REF'],
  );
  $form['VK_MSG'] = array(
    '#type' => 'hidden',
    '#value' => $vk['VK_MSG'],
  );
  $form['VK_MAC'] = array(
    '#type' => 'hidden',
    '#value' => banklink_create_signature($vk, $settings),
  );
  $form['VK_LANG'] = array(
    '#type' => 'hidden',
    '#value' => $settings['lang'],
  );
  $form['VK_RETURN'] = array(
    '#type' => 'hidden',
    '#value' => $settings['return_url'],
  );
  $form['VK_CANCEL'] = array(
    '#type' => 'hidden',
    '#value' => $settings['cancel_url'],
  );

  if (in_array($settings['bank'], array('krediidipank', 'seb', 'seb_test'))) {
    $form['VK_CHARSET'] = array(
      '#type' => 'hidden',
      '#value' => 'UTF-8',
    );
  }
  elseif (in_array($settings['bank'], array('swedbank'))) {
    $form['VK_ENCODING'] = array(
      '#type' => 'hidden',
      '#value' => 'UTF-8',
    );
  }
}

/**
 * Prepares the form fields for Maksekeskus payment form.
 */
function banklink_maksekeskus_payment_form(&$form, $settings, $payment) {
  $order_id = $payment['order_id'] ?: REQUEST_TIME;

  $vk = array(
    'shopId' => $settings['merchant_id'],
    'paymentId' => $order_id,
    'amount' => number_format($payment['amount'], 2, '.', ''),
  );

  $vk['signature'] = banklink_create_signature($vk, $settings);

  $form['json'] = array(
    '#type' => 'hidden',
    '#value' => json_encode($vk),
  );
}

/**
 * Prepares the form fields for Nordea payment form.
 */
function banklink_nordea_payment_form(&$form, $settings, $payment) {
  $order_id = $payment['order_id'] ?: REQUEST_TIME;

  $language_map = array(
    'ENG' => 3,
    'EST' => 4,
    'LAT' => 6,
    'LIT' => 7,
  );

  $vk = array(
    'SOLOPMT_VERSION' => '0003',
    'SOLOPMT_STAMP' => $order_id,
    'SOLOPMT_RCV_ID' => $settings['rcv_id'],
    // Nordea does not support russion, default to estonian.
    'SOLOPMT_LANGUAGE' => $language_map[$settings['lang']] ?: $language_map['EST'],
    'SOLOPMT_AMOUNT' => number_format($payment['amount'], 2, '.', ''),
    'SOLOPMT_REF' => banklink_get_reference_number($order_id),
    'SOLOPMT_DATE' => 'EXPRESS',
    'SOLOPMT_MSG' => $payment['message'] ?: strtr(variable_get('banklink_message', t('Order @order_id')), array('@order_id' => REQUEST_TIME)),
    'SOLOPMT_KEYVERS' => '0001',
    'SOLOPMT_CUR' => $payment['currency'] ?: 'EUR',
    'SOLOPMT_CONFIRM' => 'YES',
  );

  $form['SOLOPMT_VERSION'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_VERSION'],
  );
  $form['SOLOPMT_STAMP'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_STAMP'],
  );
  $form['SOLOPMT_RCV_ID'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_RCV_ID'],
  );
  $form['SOLOPMT_LANGUAGE'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_LANGUAGE'],
  );
  $form['SOLOPMT_AMOUNT'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_AMOUNT'],
  );
  $form['SOLOPMT_REF'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_REF'],
  );
  $form['SOLOPMT_DATE'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_DATE'],
  );
  $form['SOLOPMT_MSG'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_MSG'],
  );
  $form['SOLOPMT_RETURN'] = array(
    '#type' => 'hidden',
    '#value' => $settings['return_url'],
  );
  $form['SOLOPMT_CANCEL'] = array(
    '#type' => 'hidden',
    '#value' => $settings['cancel_url'],
  );
  $form['SOLOPMT_REJECT'] = array(
    '#type' => 'hidden',
    '#value' => $settings['reject_url'],
  );
  $form['SOLOPMT_MAC'] = array(
    '#type' => 'hidden',
    '#value' => banklink_create_signature($vk, $settings),
  );
  $form['SOLOPMT_CONFIRM'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_CONFIRM'],
  );
  $form['SOLOPMT_KEYVERS'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_KEYVERS'],
  );
  $form['SOLOPMT_CUR'] = array(
    '#type' => 'hidden',
    '#value' => $vk['SOLOPMT_CUR'],
  );
}

/**
 * Callback to be triggered when the user returns from the Bank's webpage.
 */
function banklink_return($status, $bank) {
  $settings = banklink_get_settings($bank);

  if (in_array($bank, array('maksekeskus', 'maksekeskus_test'))) {
    $data = json_decode($_POST['json'], TRUE);
  }
  elseif (in_array($bank, array('nordea', 'nordea_test'))) {
    $data = $_GET;
  }
  else {
    $data = $_POST;
  }

  $response = array(
    'bank' => $bank,
    'status' => $status,
    'verified' => banklink_verify_signature($data, $settings),
    'data' => $data,
  );

  if (in_array($bank, array('maksekeskus', 'maksekeskus_test'))) {
    $response['success'] = in_array($data['status'], array('RECEIVED', 'PAID'));
    $response['order_id'] = $data['paymentId'];
    $response['ref'] = $data['paymentId'];
    $response['amount'] = $data['amount'];
  }
  elseif (in_array($bank, array('nordea', 'nordea_test'))) {
    $response['success'] = ($status == 'return');
    $response['order_id'] = $data['SOLOPMT_RETURN_STAMP'];
    $response['ref'] = $data['SOLOPMT_RETURN_REF'];
    $response['amount'] = $_SESSION['banklink_payment']['amount'];
  }
  else {
    // 1101: success, 1901: failure.
    $response['success'] = ($data['VK_SERVICE'] == '1101');
    $response['order_id'] = $data['VK_STAMP'];
    $response['ref'] = $data['VK_REF'];
    $response['amount'] = $data['VK_AMOUNT'];
  }

  $_SESSION['banklink_response'] = $response;

  module_invoke_all('banklink_return', banklink_get_response());
}

/**
 * Returns the payment data related to the current payment.
 */
function banklink_get_response() {
  $response = $_SESSION['banklink_response'];
  // Include the original payment data for convenience.
  $response['payment'] = $_SESSION['banklink_payment'];

  return $response;
}

/**
 * Deletes all banklink session data.
 */
function banklink_clear() {
  unset($_SESSION['banklink_payment']);
  unset($_SESSION['banklink_response']);
}

/**
 * Calculates a MAC value for given payment information.
 */
function banklink_create_signature($data, $settings) {
  if (in_array($settings['bank'], array('maksekeskus', 'maksekeskus_test'))) {
    return strtoupper(hash('sha512', ($data['shopId'] . $data['paymentId'] . $data['amount'] . $settings['api_secret']), FALSE));
  }
  elseif (in_array($settings['bank'], array('nordea', 'nordea_test'))) {
    return strtoupper(call_user_func($settings['hash_function'], "{$data['SOLOPMT_VERSION']}&{$data['SOLOPMT_STAMP']}&{$data['SOLOPMT_RCV_ID']}&{$data['SOLOPMT_AMOUNT']}&{$data['SOLOPMT_REF']}&{$data['SOLOPMT_DATE']}&{$data['SOLOPMT_CUR']}&{$settings['merchant_mac_key']}&"));
  }
  else {
    $string = '';

    foreach ($data as $element) {
      $string .= str_pad(strlen($element), 3, "0", STR_PAD_LEFT) . $element;
    }

    if (empty($settings['private_key'])) {
      return;
    }

    $pkeyid = openssl_get_privatekey($settings['private_key']);
    openssl_sign($string, $signature, $pkeyid);
    $mac = base64_encode($signature);
    openssl_free_key($pkeyid);

    return $mac;
  }
}

/**
 * Verifies that the data sent back from the bank can be trusted.
 */
function banklink_verify_signature($data, $settings) {
  if (in_array($settings['bank'], array('maksekeskus', 'maksekeskus_test'))) {
    return $data['signature'] == strtoupper(hash('sha512', ($data['paymentId'] . $data['amount'] . $data['status'] . $settings['api_secret']), FALSE));
  }
  elseif (in_array($settings['bank'], array('nordea', 'nordea_test'))) {
    return $data['SOLOPMT_RETURN_MAC'] == strtoupper(call_user_func($settings['hash_function'], "{$data['SOLOPMT_RETURN_VERSION']}&{$data['SOLOPMT_RETURN_STAMP']}&{$data['SOLOPMT_RETURN_REF']}&{$data['SOLOPMT_RETURN_PAID']}&{$settings['merchant_mac_key']}&"));
  }
  else {
    $signature = base64_decode($data['VK_MAC']);

    switch ($data['VK_SERVICE']) {
      case '1101':
        $data = banklink_pad_data(array(
          $data['VK_SERVICE'],
          $data['VK_VERSION'],
          $data['VK_SND_ID'],
          $data['VK_REC_ID'],
          $data['VK_STAMP'],
          $data['VK_T_NO'],
          $data['VK_AMOUNT'],
          $data['VK_CURR'],
          $data['VK_REC_ACC'],
          $data['VK_REC_NAME'],
          $data['VK_SND_ACC'],
          $data['VK_SND_NAME'],
          $data['VK_REF'],
          $data['VK_MSG'],
          $data['VK_T_DATE'],
        ));
        break;

      case '1901':
        $data = banklink_pad_data(array(
          $data['VK_SERVICE'],
          $data['VK_VERSION'],
          $data['VK_SND_ID'],
          $data['VK_REC_ID'],
          $data['VK_STAMP'],
          $data['VK_REF'],
          $data['VK_MSG'],
        ));
        break;
    }

    $pubkey = openssl_get_publickey($settings['public_key']);
    $out = @openssl_verify($data, $signature, $pubkey);
    @openssl_free_key($pubkey);

    return $out;
  }
}

/**
 * Pads all array elements and concatenates them.
 */
function banklink_pad_data($data) {
  $out = '';

  foreach ($data as $item) {
    $out .= _banklink_str_pad($item);
  }

  return $out;
}

/**
 * Creates a reference number from input by calculating and adding a checksum.
 */
function banklink_get_reference_number($str) {
  $weights = array(7, 3, 1, 7, 3, 1, 7, 3, 1, 7, 3, 1, 7, 3, 1, 7, 3, 1, 7, 3);
  $str_a = preg_split("//", $str, -1, PREG_SPLIT_NO_EMPTY);
  $sum = 0;

  $weights = array_reverse(array_slice($weights, 0, count($str_a)));

  foreach ($str_a as $index => $num) {
    $add = $num * $weights[$index];
    $sum += $add;
  }

  if (($sum % 10) != 0) {
    $j = (10 - ($sum % 10));
  }
  else {
    $j = 0;
  }

  return $str . $j;
}

/**
 * Pad strings to the format required by Banklink standard.
 */
function _banklink_str_pad($str = "") {
  return str_pad(strlen($str), 3, "0", STR_PAD_LEFT) . $str;
}

/**
 * Defines the defaults for different payment interfaces.
 */
function banklink_get_defaults() {
  $seb_test_public_key = <<<EOD
-----BEGIN CERTIFICATE-----
MIIDRTCCAq6gAwIBAgIBADANBgkqhkiG9w0BAQQFADB7MQswCQYDVQQGEwJFRTEO
MAwGA1UECBMFSGFyanUxEDAOBgNVBAcTB1RhbGxpbm4xDDAKBgNVBAoTA0VZUDEL
MAkGA1UECxMCSVQxDDAKBgNVBAMTA2EuYTEhMB8GCSqGSIb3DQEJARYSYWxsYXIu
YWxsYXNAZXlwLmVlMB4XDTk5MTExNTA4MTAzM1oXDTk5MTIxNTA4MTAzM1owezEL
MAkGA1UEBhMCRUUxDjAMBgNVBAgTBUhhcmp1MRAwDgYDVQQHEwdUYWxsaW5uMQww
CgYDVQQKEwNFWVAxCzAJBgNVBAsTAklUMQwwCgYDVQQDEwNhLmExITAfBgkqhkiG
9w0BCQEWEmFsbGFyLmFsbGFzQGV5cC5lZTCBnzANBgkqhkiG9w0BAQEFAAOBjQAw
gYkCgYEAvgETpV4kb4tU+0PXwIdC8O97MwJ9upqnyg6negXaZat7/oFVgt79Pm4L
XzWpERP1FWDsRANM9L4qkaSsz7jcvDrLUzHvHxSrNfTVvJQWIpOjcyeAZPgelZqL
zptJyS0CcrVyoicrhz/Q6jzlZj/Evms81+1Ks6L9Z8aPX7x+8tkCAwEAAaOB2DCB
1TAdBgNVHQ4EFgQUFivCzZNmegEoOxYtg20YMMRB98gwgaUGA1UdIwSBnTCBmoAU
FivCzZNmegEoOxYtg20YMMRB98ihf6R9MHsxCzAJBgNVBAYTAkVFMQ4wDAYDVQQI
EwVIYXJqdTEQMA4GA1UEBxMHVGFsbGlubjEMMAoGA1UEChMDRVlQMQswCQYDVQQL
EwJJVDEMMAoGA1UEAxMDYS5hMSEwHwYJKoZIhvcNAQkBFhJhbGxhci5hbGxhc0Bl
eXAuZWWCAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQQFAAOBgQBfkayuot+e
fwW8QmPwpWF5AY3oMT/fTncjCljDBOg39IQv4PjnpTdDfwwl3lUIZHHTLM2i0L/c
eD4D1UFM1qdp2VZzhBd1eeMjxYjCP8qL2v2MfLkCYcP30Sl6ISSkFjFc5qbGXZOc
C82uR/wUZJDw9kj+R1O46/byG8yA+S9FVw==
-----END CERTIFICATE-----
EOD;

  $seb_test_private_key = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC+AROlXiRvi1T7Q9fAh0Lw73szAn26mqfKDqd6Bdplq3v+gVWC
3v0+bgtfNakRE/UVYOxEA0z0viqRpKzPuNy8OstTMe8fFKs19NW8lBYik6NzJ4Bk
+B6VmovOm0nJLQJytXKiJyuHP9DqPOVmP8S+azzX7Uqzov1nxo9fvH7y2QIDAQAB
AoGAFhbD9O6r57fYCloJxB01gBMnTHfWrBH8vbXUbJAvorA7+wuIKG3KHS7n7Yqs
fArI7FJXRVTo5m8RPdtaJ9ADAT9rjAi3A17TaEueyJl+B/hjHYhsd8MeFhTb2fh0
rY3F6diL8U/YDbiAIegnKO0zcc6ynJrsQZvzb6DlY/CLPe0CQQD3KXJzw1ZfJ1ts
c370b/ZC1YrRURw41Q0I8ljYJ8EJw/ngVxrnCIsd43bRnOVp9guJrjTQRkhDC3Gn
J2Y0+42LAkEAxMxmh7QY4nItBTS0fe1KCat4VDxhyxYEhZKlGDhxW75vNROrripB
1ZfBsq5xkY2MM9R7WKmL7SpStrUPIvEVqwJBAOXA4ISd61cupbytrDEbNscv7Afh
pyNpYOGVLmNYqQgj5c7WCcsD1RYmkRgPCe8y6czFZJDLFHdGVxLz+/16bTsCQC9J
Ob2TnYMTkhO1JUU4tdh69e+vjoPgp3d80+Rs83fq2wey0UaI6saqryUC21Dw5OYz
QOv92RxEVhmGibuIl/8CQCiYrzwlZJDlsKrWPZT0E8rzNmLZkhNHzYJP9S7x+FKk
m3gFeXEBgzGn9UOd6xIAp0p7A1XVBN8XzDMa09gSOks=
-----END RSA PRIVATE KEY-----
EOD;

  return array(
    'danskebank' => array(
      'name' => 'Danske Bank',
      'url' => 'https://www2.danskebank.ee/ibank/pizza/pizza',
      'plnet_url' => 'https://pangalink.net/banklink/sampo',
    ),
    'krediidipank' => array(
      'name' => 'Krediidipank',
      'url' => 'https://i-pank.krediidipank.ee/teller/maksa',
      'plnet_url' => 'https://pangalink.net/banklink/krediidipank',
    ),
    'lhvpank' => array(
      'name' => 'LHV Pank',
      'url' => 'https://www.lhv.ee/banklink',
      'plnet_url' => 'https://pangalink.net/banklink/lhv',
    ),
    'maksekeskus' => array(
      'name' => 'Maksekeskus',
      'url' => 'https://payment.maksekeskus.ee/pay/1/signed.html',
    ),
    'maksekeskus_test' => array(
      'name' => 'Maksekeskus Test',
      'url' => 'https://payment.maksekeskus.ee/pay/1/test/signed.html',
    ),
    'nordea' => array(
      'name' => 'Nordea',
      'url' => 'https://netbank.nordea.com/pnbepay/epayn.jsp',
      'plnet_url' => 'https://pangalink.net/banklink/nordea',
    ),
    'nordea_test' => array(
      'name' => 'Nordea Test',
      'url' => 'https://netbank.nordea.com/pnbepaytest/epayn.jsp',
      'rcv_id' => '12345678',
      'hash_function' => 'md5',
      'merchant_mac_key' => 'LEHTI',
    ),
    'seb' => array(
      'name' => 'SEB',
      'url' => 'https://www.seb.ee/cgi-bin/unet3.sh/un3min.r',
      'plnet_url' => 'https://pangalink.net/banklink/seb',
    ),
    'seb_test' => array(
      'name' => 'SEB Test',
      'url' => 'https://www.seb.ee/cgi-bin/dv.sh/un3min.r',
      'snd_id' => 'testvpos',
      'public_key' => $seb_test_public_key,
      'private_key' => $seb_test_private_key,
    ),
    'swedbank' => array(
      'name' => 'Swedbank',
      'url' => 'https://www.swedbank.ee/banklink',
      'plnet_url' => 'https://pangalink.net/banklink/swedbank',
    ),
  );
}
