Banklink provides an integration to the Banklink payment services provided by
Estonian commercial banks.

INSTALLATION
============
1) Enable the module from admin/build/modules
2) Set the "Configure Banklink" permission for the administrative roles.

USAGE
=====
1) Configure the bank interfaces from admin/config/system/banklink. You can
   route the requests throug pangalink.net service for testing.
2) In your module, prepare a data array containing at least the required 'bank'
   and 'amount' fields and pass it to banklink_start_transaction() to start
   the payment process.
3) Implement a hook_banklink_return() to capture and act on the response.

See the example in banklink_test module for more details.
